#include <iostream>
#include <string>
using namespace std;

int main(int argc, char const *argv[]) {
  int problem,timeToParty,baseTimeProblem=5,partyLeft=4*60,solvedProblem=0;
  cin >> problem;
  cin >> timeToParty;

  for(int x = 1;x<=problem;x++){
    timeToParty += x*baseTimeProblem;
    if(timeToParty<=partyLeft){
      solvedProblem+=1;
    }else{
      break;
    }
  }
  cout << solvedProblem << endl;
  return 0;
}
