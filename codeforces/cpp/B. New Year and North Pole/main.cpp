#include <iostream>
#include <string>
using namespace std;

int main(int argc, char const *argv[]) {
  int n,distance;
  string direction;
  int hutang = 0;
  cin >> n;
  for(int x = 0; x < n;x++){
    cin >> distance;
    cin >> direction;
    if(direction == "South"){
      hutang += distance;
    }
    else if(direction == "North"){
      if(x==0){
        hutang = 20000;
      }
      hutang -= distance;
    }else if((direction == "East" || direction == "West") &&  hutang == 20000){
      hutang+=distance;
    }else if(hutang ==0 && x==0){
      hutang +=distance;
    }
  }
  if(hutang == 0){
    cout << "YES" << endl;
  }else{
    cout << "NO" << endl;
  }
  return 0;
}
