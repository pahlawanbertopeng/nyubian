#include <iostream>
#include <string>
#include <vector>
#include <map>

using namespace std;
int number;

string UnitsMap[] = { "zero", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen", "sixteen", "seventeen", "eighteen", "nineteen" };
string TensMap[] = { "zero", "ten", "twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty", "ninety" };

int numberToWord(string locale);
void fixNumber();

string Join( string delim, vector<string> parts );
string TrimEnd(const std::string & s, std::string::value_type c);
string Convert(int number, bool isOrdinal);
string Convert(int number);
string GetUnitValue(int number, bool isOrdinal);
bool ExceptionNumbersToWords(int number, std::string & words);
string RemoveOnePrefix(const std::string & toWords);

int main()
{
    cout <<"Hello, please enter a number between -2 billion and 2 billion. \n";
    cin >> number;

    if (number < -2000000000 || number > 2000000000)
    {
        cout << "ERROR.";
    }

    else
    {
      cout << Convert(number, false) << endl;
    }

    return 0;
}

string Convert(int number)
{
    return Convert(number, false);
}

string TrimEnd(const std::string & s, std::string::value_type c)
{
    if( s.empty() )
        return s;

    std::string::size_type sz = s.size();

    if( s[sz-1] == c )
    {
        return s.substr( 0, sz - 1 );
    }

    return s;
}

string Convert(int number, bool isOrdinal)
{
    if (number == 0)
        return GetUnitValue(0, isOrdinal);

    if (number < 0)
        return "minus " + Convert(-number);

    std::vector<std::string> parts;

    if ((number / 1000000000) > 0)
    {
        parts.push_back( Convert(number / 1000000000) + " billion" );
        number %= 1000000000;
        // cout << number << endl;
    }

    if ((number / 1000000) > 0)
    {
        parts.push_back( Convert(number / 1000000) + " million" );
        number %= 1000000;
        // cout << number << endl;
    }

    if ((number / 1000) > 0)
    {
        parts.push_back( Convert(number / 1000) + " thousand" );
        number %= 1000;
        // cout << number << endl;
    }

    if ((number / 100) > 0)
    {
        parts.push_back( Convert(number / 100) + " hundred" );
        number %= 100;
        // cout << number << endl;
    }

    if (number > 0)
    {
        if (parts.size() != 0)
            parts.push_back("and");

        if (number < 20)
            parts.push_back(GetUnitValue(number, isOrdinal));
        else
        {
            auto lastPart = TensMap[number / 10];
            if ((number % 10) > 0)
                lastPart += "-" + GetUnitValue(number % 10, isOrdinal);
            else if (isOrdinal)
                lastPart = TrimEnd( lastPart, 'y' ) + "ieth";

            parts.push_back(lastPart);
        }
    }
    else if (isOrdinal)
        parts[parts.size() - 1] += "th";

    std::string toWords = Join(" ", parts);

    if (isOrdinal)
        toWords = RemoveOnePrefix(toWords);

    return toWords;
}

string Join( string delim, vector<string> parts )
{
    std::string res;

    bool is_first = true;

    for( auto & e : parts )
    {
        if( is_first )
        {
            is_first = false;
        }
        else
        {
            res += delim;
        }

        res += e;
    }

    return res;
}

string GetUnitValue(int number, bool isOrdinal)
{

        return UnitsMap[number];
}

bool ExceptionNumbersToWords(int number, std::string & words)
{


    return true;
}

string RemoveOnePrefix(const std::string & toWords)
{
    // one hundred => hundredth
    if (toWords.find("one") == 0)
        return toWords.substr(4);

    return toWords;
}
