'use strict';

const Boom = require('boom');
const Hapi = require('hapi');
const H2O2 = require('h2o2');

const proxyMap = {
  '/search/#': 'http://54.151.150.7:9191/',
  '/java/#':'http://54.251.100.162:7055/',
  '/search_dev/#': 'http://54.151.150.7:9191/',
  '/search_alpha1/#': 'http://54.151.150.7:9191/',
  '/search_alpha2/#': 'http://54.151.150.7:9191/',
  '/search_alpha3/#': 'http://54.151.150.7:9191/',
  '/search_alpha4/#': 'http://54.151.150.7:9191/',
  '/search_staging/#': 'http://54.151.150.7:9191/',
  '/java_dev/#':'http://54.251.100.162:7055/',
  '/java_alpha1/#':'http://54.251.100.162:7055/',
  '/java_alpha2/#':'http://54.251.100.162:7055/',
  '/java_alpha3/#':'http://54.251.100.162:7055/',
  '/java_alpha4/#':'http://54.251.100.162:7055/',
  '/java_staging/#':'http://54.251.100.162:7055/'
};

var server = new Hapi.Server();

server.register({ register: H2O2 }, err => {
  if (err) { return console.error(err); }

  server.connection({ port: 7000 });

  const proxyHandler = {
    proxy: {
      mapUri: (req, cb) => {
        const uri = proxyMap[req.route.fingerprint];
        // console.log(req.params.path);
        // console.log(req.route.fingerprint);

        // console.log(req.url.search);
        if (!uri) { return cb(Boom.notFound()); }
        cb(null, uri+req.params.path+req.url.search, req.headers);
      },
      onResponse: (err, res, req, reply, settings, ttl) => {
        if (err) { return reply(err); }
        reply(res);
      }
    }
  };

  server.route([
    { method: ['GET', 'POST'], path: '/search/{path*}', handler: proxyHandler },
    { method: ['GET', 'POST'], path: '/java/{path*}', handler: proxyHandler },
    { method: ['GET', 'POST'], path: '/search_dev/{path*}', handler: proxyHandler },
    { method: ['GET', 'POST'], path: '/search_alpha1/{path*}', handler: proxyHandler },
    { method: ['GET', 'POST'], path: '/search_alpha2/{path*}', handler: proxyHandler },
    { method: ['GET', 'POST'], path: '/search_alpha3/{path*}', handler: proxyHandler },
    { method: ['GET', 'POST'], path: '/search_alpha4/{path*}', handler: proxyHandler },
    { method: ['GET', 'POST'], path: '/search_staging/{path*}', handler: proxyHandler },
    { method: ['GET', 'POST'], path: '/java_dev/{path*}', handler: proxyHandler },
    { method: ['GET', 'POST'], path: '/java_alpha1/{path*}', handler: proxyHandler },
    { method: ['GET', 'POST'], path: '/java_alpha2/{path*}', handler: proxyHandler },
    { method: ['GET', 'POST'], path: '/java_alpha3/{path*}', handler: proxyHandler },
    { method: ['GET', 'POST'], path: '/java_alpha4/{path*}', handler: proxyHandler },
    { method: ['GET', 'POST'], path: '/java_staging/{path*}', handler: proxyHandler }
    // { method: 'GET', path: '/{path*}', handler: proxyHandler }
  ]);

  server.start(() => {
    console.log(`running at ${server.info.uri}`);
  });

});


function shutdown() {
  server.stop(() => console.log('shutdown successful'));
}

process
  .once('SIGINT', shutdown)
  .once('SIGTERM', shutdown);
